package com.exercise.form3.service;

import com.exercise.form3.persistence.entity.AttributesEmbeddable;
import com.exercise.form3.persistence.entity.PaymentEntity;
import com.exercise.form3.persistence.repository.PaymentRepository;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = {"com.exercise.form3.service"})
public class PaymentServiceTest {

  @Autowired private PaymentService paymentService;

  @MockBean private PaymentRepository paymentRepository;

  @Test
  public void test_ListPayments() {
    PaymentEntity paymentEntity =
        new PaymentEntity("id", "type", 3, new AttributesEmbeddable("amount"));
    PageImpl paymentEntityPage = new PageImpl(Collections.singletonList(paymentEntity));
    Mockito.when(paymentRepository.findAll(Pageable.unpaged())).thenReturn(paymentEntityPage);

    Page<PaymentEntity> actualPaymentsPage = paymentService.listPayments(Pageable.unpaged());

    Assert.assertEquals(paymentEntityPage, actualPaymentsPage);
  }

  @Test
  public void test_CreatePayment() {
    PaymentEntity paymentEntityParam =
        new PaymentEntity(null, "type", 3, new AttributesEmbeddable("amount"));
    PaymentEntity paymentEntityResult =
        new PaymentEntity("id", "type", 3, new AttributesEmbeddable("amount"));
    Mockito.when(paymentRepository.save(paymentEntityParam)).thenReturn(paymentEntityResult);

    PaymentEntity actualPayment = paymentService.createPayment(paymentEntityParam);

    Assert.assertEquals(paymentEntityResult, actualPayment);
  }

  // TODO update payment

  // TODO delete payment

  // TODO test failing scenarios such as non-existing payment ID

}
