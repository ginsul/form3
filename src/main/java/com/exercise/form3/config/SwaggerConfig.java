package com.exercise.form3.config;

import static com.google.common.collect.Sets.newHashSet;
import static springfox.documentation.schema.AlternateTypeRules.newRule;

import com.fasterxml.classmate.TypeResolver;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

  private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
      new HashSet(Arrays.asList(MediaType.APPLICATION_JSON_VALUE));

  @Autowired private TypeResolver typeResolver;

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .consumes(DEFAULT_PRODUCES_AND_CONSUMES)
        .produces(DEFAULT_PRODUCES_AND_CONSUMES)
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.exercise.form3.web.controller"))
        .paths(PathSelectors.ant(ApiMapping.Api.API_PATTERN))
        .build()
        .useDefaultResponseMessages(false)
        .globalResponseMessage(
            RequestMethod.GET,
            Arrays.asList(
                new ResponseMessageBuilder()
                    .code(404)
                    .message("Not Found")
                    .responseModel(new ModelRef("ErrorResponse"))
                    .build()))
        .genericModelSubstitutes(List.class)
        .alternateTypeRules(
            newRule(
                typeResolver.resolve(
                    DeferredResult.class,
                    typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                typeResolver.resolve(WildcardType.class)))
        .apiInfo(apiInfo())
        .pathMapping("/")
        .protocols(newHashSet("http"));
  }

  @Bean
  public SecurityConfiguration security() {
    return SecurityConfigurationBuilder.builder()
        .scopeSeparator(",")
        .useBasicAuthenticationWithAccessCodeGrant(false)
        .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        "form3 API",
        "Initial version of the form3 API.",
        "1.0.0",
        "Terms of service",
        new Contact("form3", "http://form3.io/", "support@form3.com"),
        "License of API",
        "API license URL",
        Collections.emptyList());
  }

  @Bean
  public UiConfiguration uiConfig() {
    return UiConfigurationBuilder.builder()
        .deepLinking(true)
        .defaultModelRendering(ModelRendering.EXAMPLE)
        .docExpansion(DocExpansion.NONE)
        .filter(true)
        .operationsSorter(OperationsSorter.ALPHA)
        .showExtensions(false)
        .tagsSorter(TagsSorter.ALPHA)
        .build();
  }
}
