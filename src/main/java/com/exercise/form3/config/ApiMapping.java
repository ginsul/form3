package com.exercise.form3.config;

public interface ApiMapping {

  interface Api {

    String API = "/api";
    String API_PATTERN = API + "/**";

    interface Payments {

      String PAYMENTS = Api.API + "/payments";
    }
  }
}
