package com.exercise.form3.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class ErrorResponse {

  @ApiModelProperty(value = "Error messages", required = true, example = "Some error description")
  private final String[] errors;

  public ErrorResponse(String... errors) {
    this.errors = errors;
  }
}
