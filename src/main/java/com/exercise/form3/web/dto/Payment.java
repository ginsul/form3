package com.exercise.form3.web.dto;

import com.exercise.form3.constraints.ValidationConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.Pattern;
import lombok.Data;

@ApiModel
@Data
public class Payment {

  @ApiModelProperty(readOnly = true)
  @Pattern(regexp = ValidationConstants.UUID_FORMAT, message = "{com.exercise.form3.constraints}")
  private String id;

  private String type;
  private Integer version;
  private Attributes attributes;
}
