package com.exercise.form3.web.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel
@Data
public class Attributes {

  private String amount;
}
