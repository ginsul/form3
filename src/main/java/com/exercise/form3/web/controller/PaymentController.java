package com.exercise.form3.web.controller;

import com.exercise.form3.config.ApiMapping;
import com.exercise.form3.constraints.ValidationConstants;
import com.exercise.form3.persistence.entity.PaymentEntity;
import com.exercise.form3.service.DefaultPaymentService;
import com.exercise.form3.web.dto.ErrorResponse;
import com.exercise.form3.web.dto.Payment;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.constraints.Pattern;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(
    value = ApiMapping.Api.Payments.PAYMENTS,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
@ApiResponses({@ApiResponse(code = 404, message = "Not Found", response = ErrorResponse.class)})
@RestController
public class PaymentController {

  @Autowired private final ModelMapper modelMapper;
  @Autowired private final DefaultPaymentService paymentService;

  public PaymentController(ModelMapper modelMapper, DefaultPaymentService paymentService) {
    this.modelMapper = modelMapper;
    this.paymentService = paymentService;
  }

  @GetMapping(consumes = MediaType.ALL_VALUE)
  @ApiOperation(value = "List payments")
  public Page<Payment> listPayments(Pageable pageable) {
    Page<PaymentEntity> paymentEntityPage = paymentService.listPayments(pageable);
    List<Payment> paymentList =
        modelMapper.map(
            paymentEntityPage.getContent(), new TypeToken<List<PaymentEntity>>() {}.getType());
    return new PageImpl<>(paymentList);
  }

  @PostMapping
  @ApiOperation(value = "Create a payment")
  public Payment createPayment(@Validated @RequestBody Payment payment) {
    return modelMapper.map(
        paymentService.createPayment(modelMapper.map(payment, PaymentEntity.class)), Payment.class);
  }

  @GetMapping(path = "/{paymentId}", consumes = MediaType.ALL_VALUE)
  @ApiOperation(value = "Get a payment")
  public Payment getPayment(
      @ApiParam(value = "Payment ID", required = true)
          @Pattern(
              regexp = ValidationConstants.UUID_FORMAT,
              message = "{com.exercise.form3.constraints}")
          @PathVariable
          String paymentId) {
    return modelMapper.map(paymentService.getPayment(paymentId), Payment.class);
  }

  @PutMapping(path = "/{paymentId}")
  @ApiOperation(value = "Update a payment")
  public Payment updatePayment(
      @Validated
          @ApiParam(value = "Payment ID", required = true)
          @Pattern(
              regexp = ValidationConstants.UUID_FORMAT,
              message = "{com.exercise.form3.constraints}")
          @PathVariable
          String paymentId,
      @Validated @RequestBody Payment payment) {
    PaymentEntity paymentEntity = modelMapper.map(payment, PaymentEntity.class);
    paymentEntity.setId(paymentId);
    return modelMapper.map(paymentService.updatePayment(paymentEntity), Payment.class);
  }

  @DeleteMapping(path = "/{paymentId}", consumes = MediaType.ALL_VALUE)
  @ApiOperation(value = "Delete a payment")
  public void deletePayment(
      @ApiParam(value = "Payment ID", required = true)
          @Pattern(
              regexp = ValidationConstants.UUID_FORMAT,
              message = "{com.exercise.form3.constraints}")
          @PathVariable
          String paymentId) {
    paymentService.deletePayment(paymentId);
  }
}
