package com.exercise.form3.web.controller.exception;

import com.exercise.form3.web.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class PaymentControllerAdvice {

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(code = HttpStatus.NOT_FOUND)
  public ErrorResponse handleResourceNotFound(ResourceNotFoundException e) {
    return getErrorResponse(e);
  }

  private ErrorResponse getErrorResponse(RuntimeException e) {
    return new ErrorResponse(e.getMessage());
  }
}
