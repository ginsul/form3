package com.exercise.form3.service;

import com.exercise.form3.persistence.entity.PaymentEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PaymentService {

  Page<PaymentEntity> listPayments(Pageable pageable);

  PaymentEntity getPayment(String paymentId);

  PaymentEntity createPayment(PaymentEntity payment);

  PaymentEntity updatePayment(PaymentEntity payment);

  void deletePayment(String paymentId);
}
