package com.exercise.form3.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.exercise.form3.persistence.entity.PaymentEntity;
import com.exercise.form3.persistence.repository.PaymentRepository;
import com.exercise.form3.web.controller.exception.ResourceNotFoundException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class DefaultPaymentService implements PaymentService {

  @Autowired private final PaymentRepository paymentRepository;

  public DefaultPaymentService(PaymentRepository paymentRepository) {
    this.paymentRepository = paymentRepository;
  }

  @Override
  public Page<PaymentEntity> listPayments(Pageable pageable) {
    checkNotNull(pageable);
    return paymentRepository.findAll(pageable);
  }

  @Override
  public PaymentEntity getPayment(String paymentId) {
    checkNotNull(paymentId);
    return paymentRepository
        .findById(paymentId)
        .orElseThrow(() -> new ResourceNotFoundException("No payment with ID: " + paymentId));
  }

  @Override
  public PaymentEntity createPayment(PaymentEntity payment) {
    checkNotNull(payment);
    checkArgument(payment.getId() == null);
    return paymentRepository.save(payment);
  }

  @Override
  public PaymentEntity updatePayment(PaymentEntity payment) {
    checkNotNull(payment);
    checkNotNull(payment.getId());
    if (paymentRepository.existsById(payment.getId())) {
      return paymentRepository.save(payment);
    } else {
      throw new ResourceNotFoundException("No payment with ID: " + payment.getId());
    }
  }

  @Override
  public void deletePayment(String paymentId) {
    checkNotNull(paymentId);
    if (paymentRepository.existsById(paymentId)) {
      paymentRepository.deleteById(paymentId);
    } else {
      throw new ResourceNotFoundException("No payment with ID: " + paymentId);
    }
  }
}
