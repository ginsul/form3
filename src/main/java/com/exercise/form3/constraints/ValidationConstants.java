package com.exercise.form3.constraints;

public class ValidationConstants {

  public static final String UUID_FORMAT = "^(?!.* {2})(?=\\S)(?=.*\\S$)[a-z0-9-]+$";
}
