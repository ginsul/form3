package com.exercise.form3.persistence.entity;

import com.exercise.form3.constraints.ValidationConstants;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "payments")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentEntity {

  @Id
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @GeneratedValue(generator = "uuid")
  @Pattern(regexp = ValidationConstants.UUID_FORMAT, message = "{com.exercise.form3.constraints}")
  private String id;

  private String type;
  private Integer version;
  @Embedded private AttributesEmbeddable attributes;
}
