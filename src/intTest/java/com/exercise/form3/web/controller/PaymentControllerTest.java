package com.exercise.form3.web.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.exercise.form3.Form3Application;
import com.exercise.form3.config.ApiMapping.Api.Payments;
import com.exercise.form3.persistence.entity.AttributesEmbeddable;
import com.exercise.form3.persistence.entity.PaymentEntity;
import com.exercise.form3.persistence.repository.PaymentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collections;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@Transactional
@RunWith(SpringRunner.class)
@SpringBootTest(
    classes = Form3Application.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {

  @Autowired protected WebApplicationContext webApplicationContext;

  @Autowired private PaymentRepository paymentRepository;

  MockMvc mockMvc;

  @Autowired ObjectMapper objectMapper;

  @Before
  public void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void test_listPayments() throws Exception {
    PaymentEntity savedPaymentEntity =
        paymentRepository.saveAndFlush(
            new PaymentEntity(null, "type", 1, new AttributesEmbeddable("amount")));

    PageImpl<PaymentEntity> testPage =
        new PageImpl<>(Collections.singletonList(savedPaymentEntity));

    MockHttpServletRequestBuilder requestBuilder = get(Payments.PAYMENTS);
    mockMvc
        .perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(content().json(objectMapper.writeValueAsString(testPage)));
  }

  @Test
  public void test_getPayment() throws Exception {
    PaymentEntity savedPaymentEntity =
        paymentRepository.saveAndFlush(
            new PaymentEntity(null, "type", 1, new AttributesEmbeddable("amount")));

    MockHttpServletRequestBuilder requestBuilder =
        get(Payments.PAYMENTS + "/" + savedPaymentEntity.getId());
    mockMvc
        .perform(requestBuilder)
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
        .andExpect(content().json(objectMapper.writeValueAsString(savedPaymentEntity)));
  }

  @Test
  public void test_createPayment() throws Exception {
    PaymentEntity postPaymentEntity =
        new PaymentEntity(null, "type", 1, new AttributesEmbeddable("amount"));

    PaymentEntity expectedPaymentEntity =
        new PaymentEntity(postPaymentEntity.getId(), "type", 1, new AttributesEmbeddable("amount"));

    MockHttpServletRequestBuilder requestBuilder =
        post(Payments.PAYMENTS)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(objectMapper.writeValueAsString(postPaymentEntity));
    MvcResult result =
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andReturn();

    PaymentEntity responsePaymentEntity =
        objectMapper.readValue(result.getResponse().getContentAsString(), PaymentEntity.class);

    Assert.assertNotNull(responsePaymentEntity.getId());
    responsePaymentEntity.setId(null);
    Assert.assertEquals(expectedPaymentEntity, responsePaymentEntity);
  }

  @Test
  public void test_updatePayment() throws Exception {
    PaymentEntity savedPaymentEntity =
        paymentRepository.saveAndFlush(
            new PaymentEntity(null, "type", 1, new AttributesEmbeddable("amount")));

    PaymentEntity putEntity = new PaymentEntity();
    putEntity.setType("newType");
    putEntity.setVersion(2);
    putEntity.setAttributes(new AttributesEmbeddable("amount"));

    PaymentEntity expectedEntity = new PaymentEntity();
    expectedEntity.setId(savedPaymentEntity.getId());
    expectedEntity.setType("newType");
    expectedEntity.setVersion(2);
    expectedEntity.setAttributes(new AttributesEmbeddable("amount"));

    MockHttpServletRequestBuilder requestBuilder =
        put(Payments.PAYMENTS + "/" + savedPaymentEntity.getId())
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(objectMapper.writeValueAsString(putEntity));
    MvcResult result =
        mockMvc
            .perform(requestBuilder)
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            .andReturn();

    PaymentEntity responsePaymentEntity =
        objectMapper.readValue(result.getResponse().getContentAsString(), PaymentEntity.class);

    Assert.assertEquals(expectedEntity, responsePaymentEntity);
  }

  @Test
  public void test_deletePayment() throws Exception {
    PaymentEntity savedPaymentEntity =
        paymentRepository.saveAndFlush(
            new PaymentEntity(null, "type", 1, new AttributesEmbeddable("amount")));

    MockHttpServletRequestBuilder requestBuilder =
        delete(Payments.PAYMENTS + "/" + savedPaymentEntity.getId());
    mockMvc.perform(requestBuilder).andExpect(status().isOk());
  }

  // TODO add test with expected failures, e.g. missing payment ID

}
