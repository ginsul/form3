package com.exercise.form3.persistence.repository;

import com.exercise.form3.persistence.entity.AttributesEmbeddable;
import com.exercise.form3.persistence.entity.PaymentEntity;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PaymentRepositoryTest {

  @Autowired private TestEntityManager entityManager;

  @Autowired private PaymentRepository paymentRepository;

  @Test
  public void testFindAllPayments() {
    PaymentEntity paymentEntity =
        new PaymentEntity("id", "type", 2, new AttributesEmbeddable("amount"));
    paymentEntity.setId(null);
    entityManager.persist(paymentEntity);
    entityManager.flush();

    Optional<PaymentEntity> foundOptionalPaymentEntity =
        paymentRepository.findById(paymentEntity.getId());
    Assert.assertTrue(foundOptionalPaymentEntity.isPresent());

    PaymentEntity foundPaymentEntity = foundOptionalPaymentEntity.get();
    Assert.assertTrue(paymentEntity.equals(foundPaymentEntity));
  }

  @Test
  public void testSavePayment() {
    PaymentEntity paymentEntity =
        new PaymentEntity(null, "type", 2, new AttributesEmbeddable("amount"));

    PaymentEntity savedPaymentEntity = paymentRepository.saveAndFlush(paymentEntity);
    PaymentEntity persistedPayment = entityManager.find(PaymentEntity.class, paymentEntity.getId());
    Assert.assertTrue(savedPaymentEntity.equals(persistedPayment));
  }

  // TODO test the rest of the repository operations

}
